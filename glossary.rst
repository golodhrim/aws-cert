********
Glossary
********

.. glossary::

   Availability Zones as a Data Center

     - Found everywhere in the world, including in a city.
     - Several servers, switches, load balancing and firewalls.
     - An availability zone can consist of multiple data centers

   Region

     - A geographical area is referred to as a region.
     - Collection of data centers that are geographically separated from one another.
     - Made up of more than two availability zones that are linked together.
     - Metro fibres that are redundant and are separated, connect the availability zones.

   Edge locations

     - The endpoints for AWS content caching are known as edge locations.
     - Regions aren't the only thing that may be found in the outside. There are currently over 150 edge sites.
     - AWS has a small place called an edge location that is not a region. It's used to store content in a cache.

   Regional Edge Caches

     - In November 2016, AWS unveiled a new sort of edge location called a Regional Edge Cache.
     - Between CloudFront Origin servers and edge locations is a Regional Edge Cache.
     - At the edge site, data is taken from the cache, but data is maintained at the Regional Edge Cache.
