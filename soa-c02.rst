******************************************************
AWS Certified SysOps Administrator Associate (SOA-C02)
******************************************************

Overview of AWS SysOps
######################

Overview
*********

What is Cloud Computing?
========================

Cloud computing refers to the provision of computing resources as a service, with the cloud provider owning and managing the resources rather than the end-user.

It is:

- Browser-based software programmes
- Third-party data storage
- Third-party servers that support a business, research or personal project's computing infrastructure.

Before the widespread adoption of cloud computing, businesses and regular computer users had to purchase and maintain the software and hardware needed.

By moving away from on-premis software and hardware, cloud customers can avoid investing the time, money and skills required to purchase and manage these computing resources.

This unparalleled access to computing resources has spawned a new wave of cloud-based enterprises, altered IT practices across industries, and revolutionized many routine computer-assisted activities.

Individuals can now collaborate with other workers via:

- Video meetings and other collaborative platforms.
- Access on-demand entertainment and instructional content.
- Speak with household appliances.
- Hail a cab with a mobile device.
- Rent a vacation room in someone's home.

Scope of Cloud Computing
========================

Cloud computing is defined by the National Institute of Standards and Technology (NIST). NIST is a non-regulatory agency of the United States Department of Commerce to advance innovation.

According to NIST there are five core properties of Cloud Computing:

:Self-service on-demand: Consumers can get quick access to cloud services after signing up with this strategy.

  Organizations can also set up systems that employees, customers, or partners to use internal cloud services on demand based on established logics without to go via IT.

:Broad access to the internet: Users that have permission can access cloud services and resources from any device and any networked location.

:Pooling of resources: Cloud Users can choose from a pool of "hardware" what is needed.

:Elasticity that responds quickly: The infrastructure can respond quickly to changed demands.

:Service that is measured: You only pay for what you use.

History of Cloud Computing
==========================

:1950s: Universities renting Compute Resources

  Renting was one of the only methods to get access to computing resources at the time.

:1960s: John McCarthy of Stanford University and J.C.R. Licklider of the US Department of Defense Advanced Research Projects Agency (ARPA) proposed the public utility and the possibility of a network of computers that would allow people to access data and programs from anywhere on the planet.

:2006: Amazon's Elastic Compute (EC2) and Simple Storage Service (S3)

:2007: Heroku

:2008: Google Cloud Platform (GCP)

:2009: Alibaba Cloud

:2010: Microsoft Azure (fka Windows Azure), IBM SmartCloud, and DigitalOcean

:2011: Windows Azure renamed to Microsoft Azure

Cloud Computing's Benefits
==========================

- Cost
- Speed
- Scalability
- Productivity
- Reliability
- Security

Types of Cloud Computing
========================

:Public Cloud: Owned and maintained by a third-party cloud service provider.

:Private Cloud: Exclusively used within a single business or an organization.

:Hybrid Cloud: Linked by technology to allow data applications to be exchanged across them.

Overview of Cloud Services
==========================

:IaaS: Infrastructure as a Service

:PaaS: Platform as a Service

:SaaS: Software as a Service

Introduction to AWS
*******************

What is AWS?
============

AWS is Amazon's comprehensive cloud computing platform, includes IaaS, PaaS, SaaS offerings.

Amazon Web Services (AWS) was established in 2006 as a complement to Amazon.com's own infrastructure for managing its online retail activities.

AWS was one of the first companies who offered a pay-as-you-go cloud computing model.

It offers a number of tools and solutions for enterprises and software developers, that can be used in data centers all over the world.

Who can use AWS:

- Government agencies
- Educational institutions
- Nonprofits
- Private enterprises

These tools are divided in over 100 services including Compute, Databases, Infrastructure management, Application development, Security and many more.

AWS Global Infrastructure
=========================

#. :term:`Availability Zones as a Data Center`:

  - Found everywhere in the world, including in a city.
  - Several servers, switches, load balancing and firewalls.
  - An availability zone can consist of multiple data centers

#. :term:`Region`:

  - A geographical area is referred to as a region.
  - Collection of data centers that are geographically separated from one another.
  - Made up of more than two availability zones that are linked together.
  - Metro fibres that are redundant and are separated, connect the availability zones.

#. :term:`Edge locations`:

  - The endpoints for AWS content caching are known as edge locations.
  - Regions aren't the only thing that may be found in the outside. There are currently over 150 edge sites.
  - AWS has a small place called an edge location that is not a region. It's used to store content in a cache.

#. :term:`Regional Edge Caches`:

  - In November 2016, AWS unveiled a new sort of edge location called a Regional Edge Cache.
  - Between CloudFront Origin servers and edge locations is a Regional Edge Cache.
  - At the edge site, data is taken from the cache, but data is maintained at the Regional Edge Cache.

Monitoring, metrics, and logging
################################

What are Metrics?
*****************

Metrics are raw measurements of resource usage or behavior that can be viewed and collected across your systems.

Tey are the fundamental values to understand your systems health and system itself.

What is Monitoring?
*******************

While metrics reflect the data in your system, monitoring is the process of gathering, aggregating, and evaluating those values in order to gain a better understanding of the features and behavior of your components.

Types of Data to Keep Track
***************************

#. :Host-Based Metrics: The indicators would include anything related to assessing the health or performance of a single machine, ignoring its application stacks and services for the time being.
#. :

Cloud Watch
***********

Amazon EC2
**********

EC2 Launch
**********

How to launch Linux based instance
**********************************

Amazon Relational Database Services
***********************************

Elastic file system
*******************

Cloud Trail
***********

AWS config
**********

How to setup and config and rules for AWS config
************************************************

Deployment and Provisioning
###########################

Instance Types
**************

:General Purpose:
    represented by **T**

:Compute Optimized:
    represented by **C**

:Memory Optimized:
    represented by **X**

:Accelerated Computing:
    represented by **B**

:Storage Optimized:
    represented by **H**

Storage/EBS Types
*****************

#. General Purpose SSD
    - It is most suited for general purpose where it balances cost and performance.
    - It supports a variety of workloads.
    - It can be used as the root volume of EC2 instances.
    - Mostly used for Low-latency interactive applications and Development and test environments.
    - It provides Baseline performance of 3 IOPS per GiB.
    - The minimum IOPS it provides is 100 and maximum is 10,000

    (GiB is Gigibytes, this means 1 Gigibyte (GiB) = 1.074 Gigabytes (GB))
#. Provisioned IOPS SSD
    - For applications that demands extensively high-throughput and lowest latency.
    - Suited for applications that require sustained IOPS performance, more than 10,000 IOPS, or 160 MiB/s of throughput per volume.
    - Used in scenarios where database workloads are very high.
    - It provides Baseline performance of 50 IOPS per GiB of volume.
    - The minimum IOPS it provides is 100 and maximum is 32,000
#. Magnetic HDD
    - Older generation volumes are backed by magnetic drives and are suited for workloads where data is accessed infrequently.
    - Suited for applications where low-cost storage for small volume sizes is important.
    - These volumes deliver approximately 100 IOPS on average and max throughput is 90 MiB/s.
#. Other HDD options:
    - Throughput Optimized HDD
        - For applications that have frequently accessed throughput-intensive workloads.
        - Low cost HDD option.
        - Max throughput per volume is 500 MB/s
    - Cold HDD
        - For applications that have less frequently accessed non-intensive workloads.
        - Lowest cost HDD option.
        - Max throughput per volume is 250 MB/s

    None of these other HDD options can be used as a root volume for EC2 instances.

:Differences between SSD and HDD:
    - SSD-backed volumes are optimized and more suited for applications that require frequent r/w operations with small I/O size.
    - HDD-backed volumes are more useful when throughput (MiB/s) is more critical than IOPS.


Autoscaling
***********

How does Autoscaling decides which Instance to drop?
====================================================

.. graphviz::

    digraph G {
        graph [splines=ortho];

        ScaleIn [
            label = "Scale in";
            shape = oval;
            style = filled;
            fillcolor = "aqua";
        ];
        MAZ [
            label = "Are there\ninstances in\multiple\nAZs?";
            shape = diamond;
            style = filled;
            fillcolor = "moccasin";
        ];
        SAZ [
            label = "Select the AZ with the\nmost instances";
            shape = rect;
            style = filled;
            fillcolor = "lightskyblue";
        ];
        SOLC [
            label = "Select the<D-/>ninstances\nwith the\noldest\nlaunch\nconfig.";
            shape = rect;
            style = filled;
            fillcolor = "lightskyblue";
        ];
        MOLC [
            label = "Are there\nmultiple\ninstances\nusing the\noldest\nlaunch\nconfig?";
            shape = diamond;
            style = filled;
            fillcolor = "moccasin";
        ];
        SCBH [
            label = "Select the\ninstance\closest to\nthe next\nbilling hour.";
            shape = rect;
            style = filled;
            fillcolor = "lightskyblue";
        ];
        MCBH [
            label = "Are there\nmultiple\ninstances\nclosest to\nthe next\nbilling hour?";
            shape = diamond;
            style = filled;
            fillcolor = "moccasin";
        ];
        SR [
            label = "Select an\ninstance at\nrandom.";
            shape = rect;
            style = filled;
            fillcolor = "lightskyblue";
        ];
        Term [
            label = "Terminate instance";
            shape = oval;
            style = filled;
            fillcolor = "red";
        ];

        ScaleIn -> MAZ;
        MAZ -> SAZ [ label = "Yes" ];
        MAZ -> SOLC [ label = "No" ];
        SAZ -> SOLC;
        SOLC -> MOLC;
        MOLC -> SCBH [ label = "Yes" ];
        MOLC -> Term [ label = "No" ];
        SCBH -> MCBH;
        MCBH -> SR [ label = "Yes" ];
        MCBH -> Term [ label = "No" ];
        SR -> Term;
        {
            rank=same;
            ScaleIn; MAZ; SOLC; MOLC; SCBH; MCBH; SR;
        }
    }

AWS RDS vs. AWS Aurora
**********************

AWS RDS
=======

- Multi AZ, able to run as Master-Master
- read replicas
- Aurora itself only read replicas no Master-Master

High Availability
#################

Network vs Application LoadBalancer
***********************************

.. table:: ALB vs ELB

  +-------------------------------------------+-----------------------+--------------------+
  | Parameter                                 | ALB                   | NLB                |
  +===========================================+=======================+====================+
  | Operates at OSI Layer                     | Layer 7 (HTTP, HTTPS) | Layer 4 (TCP)      |
  +-------------------------------------------+-----------------------+--------------------+
  | Cross-Zone Load Balancing                 | Always Enabled        | Disable by default |
  +-------------------------------------------+-----------------------+--------------------+
  | SSL Offloading                            | Supported             | not Supported      |
  +-------------------------------------------+-----------------------+--------------------+
  | Client Request terminates at LB?          | Yes                   | No                 |
  +-------------------------------------------+-----------------------+--------------------+
  | Headers modified?                         | Yes                   | No                 |
  +-------------------------------------------+-----------------------+--------------------+
  | Host based routing and path based routing | Yes                   | No                 |
  +-------------------------------------------+-----------------------+--------------------+
  | Static IP Address for Load Balancer       | Not supported         | Supported          |
  +-------------------------------------------+-----------------------+--------------------+

Networking
##########

Reserved IP-addresses by VPC
****************************

Each VPC reserve the following 5 IP-addresses by default in each Subnet.

For example: in a subnet with CIDR block 10.0.0.0/24, the following five IP addresses are reserved:

- 10.0.0.0: Network address
- 10.0.0.1: Reserved by AWS for the VPC router
- 10.0.0.2: Reserved by AWS: The IP address of the DNS server is always the base of the VPC network range plus two; however, we also reserve the base of each subnet range plus two. For VPCs with multiple CIDR blocks, the IP address of the DNS server is located in the primary CIDR.
- 10.0.0.4: Reserved by AWS for future use
- 10.0.0.255: Network broadcast address. We do not support broadcast in a VPC, therefore we reserve this address.

:RT-Recommendation by AWS:
  For Routetables (RT) do not add an IGW (Internet GateWay) to the default RT. Each VPC will associate a default RT with itself. Use this RT as a private RT, if you need a public RT create a new one, add the subnet-association to it and add the IGW to this RT as a route. As new subnets that are being created will automatically be added to the default RT, and that way one will have more control over the subnets.

AWS VPC-Wizard
**************

The AWS VPC-Wizard has four options for VPC creation and follows above mentioned recommendation.

Options are:

- VPC with a Single Public Subnet
- VPC with Public and Private Subnets (NAT)
- VPC with Public and Private Subnets and AWS Managed VPN Access
- VPC with a Private Subnet Only and AWS Managed VPN Access

NAT Instance vs. NAT Gateway
****************************

.. table:: NAT Gateway vs NAT Instance

  +-------------------------------------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------+
  | NAT Gateway                                                                                                             | NAT Instance                                                                                                                 |
  +=========================================================================================================================+==============================================================================================================================+
  | PROS: Managed by AWS, implicitly highly available and scalable                                                          | PROS: Customizable, User in control of creating and managing, Multiple instances needed to be highly available and scalable. |
  +-------------------------------------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------+
  | Least likely to be a single point of failure                                                                            | Can become a single point of failure                                                                                         |
  +-------------------------------------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------+
  | Uniform offering by AWS                                                                                                 | Flexibility in the size and type.                                                                                            |
  +-------------------------------------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------+
  | Cannot be used as a Bastion Server                                                                                      | Can be used as a Bastion Server                                                                                              |
  +-------------------------------------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------+
  | Port Forwarding is not supported                                                                                        | Port Forwarding is supported                                                                                                 |
  +-------------------------------------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------+
  | Does not support fragmentation for the TCP and ICMP protocols. Fragmented packets for these protocols will get dropped. | Supports reassembly of IP fragmented packets for the UDP, TCP, and ICMP protocols.                                           |
  +-------------------------------------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------+

VPC Peering and VPC Endpoints
*****************************

VPC Peering
===========

- A VPC peering connection is a networking connection between two VPCs that enables you to route traffic between them using **private** IPv4 or IPv6 addresses.
- Can be created between your own VPCs, with a VPC in another AWS account, or with a VPC in a different AWS Region.
- AWS uses the existing infrastructure of a VPC to create a VPC peering connection; it is neither a gateway nor a VPN connection, and does not rely on a separate piece of physical hardware.
- There is no single point of failure for communication or bandwidth bottleneck.
- Helps facilitating the transfer of data.
- You cannot edit the VPC peering connection once it is created.

.. uml::

    participant "VPC\nrequester VPC\n172.16.0.0/24" as requester #MediumTurquoise
    participant "VPC\nacceptor VPC\n10.0.1.0/24" as acceptor #Crimson

    requester -[#blue]> acceptor: Peering Connection Requested
    requester <[#green]- acceptor: Peering Connection Accepted
    requester <-[#orange]> acceptor: pcx-123456 Peering Connection

.. table:: VPC Peering RouteTable

  =============  ============  ===============  ============
         Requestor VPC                  Acceptor VPC
  ---------------------------  -----------------------------
   Destination    Target        Destination      Target
  =============  ============  ===============  ============
   10.0.1.0/24    pcx-123456    172.16.0.0/24    pcx-123456
  =============  ============  ===============  ============

Peering Connections **are not** transitive!

VPC Endpoints
=============

- Virtual devices that enable you to privately connect your VPC to supported AWS services and VPC endpoint services powered by PrivateLink without requiring an internet gateway, NAT device, VPN connection, or AWS Direct Connect connection.
- Instances in your VPC do not require public IP addresses to communicate with resources in the service.
- Traffic between your VPC and the other service does not leave the AWS network.
- Horizontally scaled, redundant, and highly available VPC components.
- Two types:

  - Interface Endpoints

    - An elastic network interface with a private IP address that serves as an entry point. E.g. AWS CloudFormation, AWS CloudWatch etc.

  - Gateway Endpoints

    - A gateway that is a target for a specified route in your route table. E.g. Amazon S3, DynamoDB etc.

VPC Flow Logs
*************

Creation on three levels:

- VPC
- Subnet
- Interface

Route 53
********

Simple Routing Policy
*********************

- Traffic is routed to a single resource. E.g. to an ELB or an IP address of a webserver
- not able to create multiple records that have the same name and type,
- multiple values in the same record, such as multiple IP addresses are possible

  - Route 53 will return all values in random order to the recursive resolver and the resolver to the client (like a web browser) that submitted the DNS query

- Two types of Records:

  :Alias Record:

  - The endpoint will be an AWS resource, such as ELB, CloudFront Distribution etc.
  - You can attach a health check with this record.

  :Basic/Non-Alias Record:

  - You specify the IP addresses of individual webservers.
  - You can't attach a health check to the "basic/non-aliased" simple routing policy.

Weighted Routing Policy
***********************

- Let's you associate multiple resources with a single domain name or subdomain name and choose how much traffic is routed to each resource.
- Create records that have the same name and type for each of your resources
- Assign each record a relative weight that corresponds with how much traffic you want to send to each resource.
- :math:`\text{Traffic routed to a resource} = \frac{\text{Weight of the specific record}}{\text{Sum of the weights for all records}}`. As percentage this would be :math:`\text{Traffic routed to a resource in %} = \frac{\text{Weight of the specific record}}{\text{Sum of the weights for all records}}\cdot 100\%`
- Possible Scenario: You want to test a new version of the software and route only some portion of the traffic to the resources having the new version and remaining portion to the old production version.

Geographical Routing Policy
***************************

- Traffic is routed based on the geographic location of the origin of DNS query.
- E.g. Traffic coming from an IP address in the USA will be routed to a resource defined in the us-east-1 region, traffic coming from an IP address in Singapore will be routed to the resource in the ap-southeast-1 region.
- If Route 53 cannot identify the location of the DNS query origin, it routes the traffic according to the **default** record. If there is no default record, Route 53 returns "no answer" response for queries from those locations.
- Possible Scenario: To localize the content and present some or all of the website in the language of the users, to restrict distribution of content to users from specific countries etc.
- Test settings with the *Test Record Set* feature of Route 53. You can use there the following List of IP Address Blocks found at https://www.nirsoft.net/countryip/.

Latency Routing Policy
**********************

- Helps to improve performance by serving their requests from the AWS Region that provides the lowest latency.
- How does this work?

  #. Create latency records for the resources in multiple AWS Regions.
  #. When Route 53 receives a DNS query for the domain, it determines which region gives the user the lowest latency, and then selects a latency record for that region.
  #. Route 53 responds with the value from the selected record, such as the IP address for a web server.

- Possible Scenario: Any application that is deployed in multiple regions and requires response from the resource in milliseconds to microseconds. E.g. Streaming Media or online gaming applications.

Failover Routing Policy
***********************

- Lets you route traffic to primary resource when the resource is healthy and to secondary resource when the first resource is unhealthy.
- Possible Scenario: An application hosted on a hybrid cloud. If your instance in cloud fails, the traffic gets routed to an on-prem webserver.
- An important feature of this policy is the **Health Check**:

  - Monitors the health and performance of your web applications, webservers, and other resources.
  - It monitors:

    - The health of a specified resource, such as a webserver
    - The status of other health checks
    - The status of an Amazon CloudWatch alarm.

  - After you create a health check, you can get the status of the health check and configure DNS failover.
  - You can configure an Amazon CloudWatch alarm for each health check.

Multivalue Answer Routing Policy
********************************

- Lets you configure Amazon Route 53 to return multiple values (e.g. IP Addresses) in response to DNS queries.
- Different than Simple Routing Policy: Lets you check the health of each resource, so Route 53 returns only values for healthy resources.
- It's not a substitute for a load balancer.
- Route 53 responds to DNS queries with up to eight healthy records and gives different answers to different DNS resolvers.
- Note:

  - If you associate a health check, Route 53 responds to DNS queries with the corresponding IP address only when the health check is healthy.
  - If you don't associate a health check, Route 53 always considers the record to be healthy.
  - If you have eight or fewer healthy records, Route 53 responds to all DNS queries with all the healthy records.
  - When all records are unhealthy, Route 53 responds to DNS queries with up to eight unhealthy records.

- Possible Scenario: Virtually any web application that is hosted on multiple servers.

Geoproximity Routing Policy
***************************

- Traffic is routed to resources based on the geographic location of your users and resources.
- You can choose to route more or less traffic to a given resource by specifying a value, known as a *bias*. A bias expands or shrinks the size of the geographic region from which traffic is routed to a resource.
- To use geoproximity routing, you must use **Route 53 traffic flow**. You create geoproximity rules for your resources and specify one of the following values for each rule:

  - If you're using AWS resources, the AWS Region that you created the resource in.
  - If you're using non-AWS resources, the latitude and longitude of the resource.

- Optionally change the size of the geographic region from which Route 53 routes traffic to a resource, specify the applicable value for the bias:

  - To expand the size of the geographic region from which Route 53 routes traffic to a resource, specify a positive bias/integer from 1 to 99. Route 53 shrinks the size of adjacent regions.
  - To shrink the size of the geographic region from which Route 53 routes traffic to a resource, specify a negative bias/integer from -1 to -99. Route 53 shrinks the size of adjacent regions.

- Effect of changing the bias for your resources depends on a number of factors, including the following:

  - The number of resources that you have.
  - How close the resources are to one another.
  - The number of users that you have near the border area between geographic regions. For example, suppose you have resources in the AWS regions USEast (Northern Virginia) and USWest (Oregon) and you have a lot of users in Dallas, Austin, and San Antonio, Texas, USA. Those cities are roughly equidistant between your resources, so a small change in bias could result in a large swing in traffic from resources in one AWS region to another.

Formulas that AWS Route 53 uses to determine how to route traffic:

:Positive bias: :math:`\text{Biased distance}=\text{actual distance}\cdot\left(1-\frac{\text{bias}}{100}\right)`
:Negative bias: :math:`\text{Biased distance}=\frac{\text{actual distance}}{1+\frac{\text{bias}}{100}}`

Here is the calculation for a positive bias of 50 and an actual distance of 150 km:

.. math::

  \text{bias} &= 50\\
  \text{actual distance} &= 150\text{km}\\
  \text{Biased distance} &= \text{actual distance}\cdot\left(1-\frac{\text{bias}}{100}\right)\\\\
  \text{Biased distance} &= 150\text{km}\cdot\left(1-\frac{50}{100}\right)\\
  \text{Biased distance} &= 150\text{km}\cdot\left(1- 0.50\right)\\
  \text{Biased distance} &= 150\text{km}\cdot 0.50\\
  \text{Biased distance} &= 75\text{km}

And here is the calculation for a negative bias of -50 and an actual distance of 150 km:

.. math::

  \text{bias} &= -50\\
  \text{actual distance} &= 150\text{km}\\
  \text{Biased distance} &= \frac{\text{actual distance}}{1+\frac{\text{bias}}{100}}\\\\
  \text{Biased distance} &= \frac{150\text{km}}{1+\frac{-50}{100}}\\
  \text{Biased distance} &= \frac{150\text{km}}{1+(-0.50)}\\
  \text{Biased distance} &= \frac{150\text{km}}{1-0.50}\\
  \text{Biased distance} &= \frac{150\text{km}}{0.50}\\
  \text{Biased distance} &= 150\text{km}\cdot 2\\
  \text{Biased distance} &= 300\text{km}

Monitoring and Reporting
########################

Overview Monitoring and Reporting
*********************************

- Why is Monitoring required?
- How does Amazon CloudWatch work?
- How to achieve monitoring with CloudWatch?
- Needed to:

  - Monitoring compute layer (EC2, EBS, ELB) for usage and outages.
  - Monitoring AWS S3 for storage and request metrics.
  - Monitoring Billing and create alarms for excess usage.

- What is CloudWatch and how does it work?

  - Amazon CloudWatch is a monitoring and management service.
  - CloudWatch provides you with data and actionable insights to monitor your applications, understand and respond to system-wide performance changes, optimize resource utilization, and get a unified view of operational health.
  - What it does:

    :Collect:

      - Logs,
      - Metrics,

        - two types:

          - Default Metrics:

            .. table:: Default Metrics

              +---------+------------------------------+
              | CPU     | - CPUUtilization             |
              +---------+------------------------------+
              | Disk    | - DiskReadOps                |
              |         | - DiskWriteOp                |
              |         | - DiskReadBytes              |
              |         | - DiskWriteBytes             |
              +---------+------------------------------+
              | Network | - NetworkIn                  |
              |         | - NetworkOut                 |
              |         | - NetworkPacketIn            |
              |         | - NetworkPacketOut           |
              +---------+------------------------------+
              | Status  | - StatusCheckFailed          |
              |         | - StatusCheckFailed_instance |
              |         | - StatusCheckFailed_System   |
              +---------+------------------------------+


          - Custom Metrics:

            .. table:: Custom Metrics

              +---------+----------------------------+
              | Memory  | - mem-util                 |
              |         | - mem-used                 |
              |         | - mem-used-incl-cache-buff |
              |         | - mem-avail                |
              +---------+----------------------------+
              | Swap    | - swap-util                |
              |         | - swap-used                |
              +---------+----------------------------+
              | Disk    | - disk-space-util          |
              |         | - disk-space-used          |
              |         | - disk-space-avail         |
              +---------+----------------------------+

            Scripts need to be run on the EC2-Host.

      - Events.

    :Monitor:

      - Dashboards

        - Monitor your resources in a single view.
        - An operational playbook that provides guidance for team members during operational events about how to respond to specific incidents.
        - A common view of critical resource and application measurements that can be shared by team members for faster communication flow during operational events.

      - Alarms.

        - The alarm performs one or more actions based on the value of the metric relative to a threshold over a number of time periods.
        - You can also add alarms to CloudWatch dashboards and monitor them visually.

      Two types of monitoring:

      :Basic Monitoring:

        - Enabled by default.
        - Data is available automatically in 5-minute periods at no change.

      :Detailed Monitoring:

        - Must be specifically enabled at the instance level.
        - Data is available in 1-minute periods for an additional cost.

    :Act:

      - on Alarms,

        - Auto-Scaling group actions
        - EC2 actions

      - on Events.

        - Events - An event indicates a change in your AWS environment, such as EC2 instance state change from pending to running.
        - Targets - A target processes events. Targets can include Amazon EC2 instances, AWS Lambda functions
        - Rules - A rule matches incoming events and routes them to targets for processing. A single rule can route to multiple targets, all of which are processed in parallel.

    :Analyze:

      - Search and understand problems and metrics with a unified view of Operational Health.
      - Process CloudWatch log events through Kinesis and Lambdas for custom processing and analysis.
      - Metric math enables you to query multiple CloudWatch metrics and use math expressions to create new time series based on these metrics. You can visualize the resulting time series in the CloudWatch console and add them to dashboards.

CloudWatch EC2 custom metrics and monitoring
********************************************

- Minimal requirements in the role policy for logging to CloudWatch:

  - cloudwatch:PutMetricData
  - cloudwatch:GetMetricStatistics
  - cloudwatch:ListMetrics
  - ec2:DescribeTags

- .. code-block:: bash
     :linenos:

     # Install packages
     $ sudo yum install -y perl-Switch perl-DateTime perl-Sys-Syslog perl-LWP-Protocol-https perl-Digest-SHA.x86_64

     # Download monitoring scripts
     $ curl https://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.2.zip -O

     # Install monitoring scripts
     $ unzip CloudWatchMonitoringScripts-1.2.2.zip && \
     $ rm CloudWatchMonitoringScripts-1.2.2.zip && \
     $ cd aws-scripts-mon

     # Perform a simple test without pushing to CloudWatch
     $ ./mon-put-instance-data.pl --mem-util --verify --verbose

     # Push to CloudWatch manually
     $ ./mon-put-instance-data.pl --mem-used-incl-cache-buff --mem-util --mem-used --mem-avail

     # Scheduling metrics report to CloudWatch
     $ crontab -e
     */1 * * * * ~/aws-scripts-mon/mon-put-instance-data.pl --mem-used-incl-cache-buff --mem-util --mem-used --mem-avail --disk-space-util --disk-path=/ --from-cron

     # To Get utilization statistics on the instance terminal
     $ ./mon-get-instance-stats.pl --recent-hours=12

EBS Monitoring
**************

- Data is only reported to CloudWatch when the volume is attached to an instance.
- CloudWatch collects metrics.
- CloudWatch performs Status Checks.
- Customers can set alarms to get notified on the usage of EBS volumes.

.. table:: Basic vs Detailed Monitoring

  +-------------------------------------+-----------------------------+
  | Basic Monitoring                    | Detailed Monitoring         |
  +=====================================+=============================+
  | 5 minute periods                    | 1 minute periods            |
  +-------------------------------------+-----------------------------+
  | Amazon EBS General Purpose SSD(gp2) | Provisioned IOPS SSD (io1)  |
  | Throughput Optimized (st1)          |                             |
  | Cold HDD (sc1)                      |                             |
  | Magnetic (standard)                 |                             |
  +-------------------------------------+-----------------------------+

.. warning:
    For EBS Volumes you can not switch between the above monitoring levels. They are fixed values.

ELB Monitoring
**************

- ELB types

  - Application Load Balancer (ALB, Layer 7)
  - Network Load Balancer (NLB, Layer 4, TCP/IP and TLS)
  - Classic Load Balancer (both Layer 4 and 7, but deprecating)

- Types of monitoring ELB

  - CloudWatch metrics
  - Access logs
  - Request tracing
  - CloudTrail logs


Creating Dashboards and Alarms in CloudWatch
********************************************

- Discussed before

AWS Trusted Advisor
*******************

AWS Trusted Advisor helps you provision your resources by following best practices. It inspects your AWS environment and finds opportunities to save money, improves performance and reliability, or helps you to close security gaps.

Trusted Advisor covers the following five fields:

:Cost Optimization:

  - Under utilized EC2 instances
  - Idle Elastic Load Balancers
  - Unassociated Elastic IPs

:Performance:

  - Highly utilized EC2 instances
  - Rules in EC2 security groups
  - Over utilized EBS Volumes

:Security:

  - Security Groups unrestricted access.
  - IAM Password Policy

:Fault Tolerance:

  - EC2 instance distribution across AZs in a region
  - AWS RDS Multi AZ

:Service Limits:

  - Service limits on AWS VPC, EBS, IAM, S3 etc.

.. figure:: images/ta-symbol.png
   :alt: TA Symbol

   Trust Advisor Symbol

:red:`Red`: action recommended.

:orange:`Yellow`: investigation recommended

:green:`Green`: no problem detected

The 4 different Support Plans give you different access to Trusted Advisor:

.. table:: Support Plans

  +------------------------+------------------------+-----------------------+-----------------------+
  | Basic Support          | Developer Support      | Business Support      | Enterprise Support    |
  +========================+========================+=======================+=======================+
  | Access to 7 core       | Access to 7 core       | Access to all Trusted | Access to all Trusted |
  | Trusted Advisor Checks | Trusted Advisor Checks | Advisor Checks        | Advisor Checks        |
  +------------------------+------------------------+-----------------------+-----------------------+

The **7 core Trusted Advisor Checks** are:

:Security:

  - S3 Bucket Permissions
  - Security Groups - Specific Ports unrestricted
  - IAM Use
  - MFA on Root Account
  - EBS Public Snapshots
  - RDS Public Snapshots

:Service Limits:

AWS Resource Groups
*******************

- A resource group is a collection of AWS resources that are all in the same AWS region, that match criteria provided in a query, and that share one or more tags or portions of tags.
- Tagging

  - A tag is a label that you assign to an AWS resource and is useful when you have many resources of the same type.
  - A tag is a Key-Value-Pair.
  - Useful for Patching, Run commands, Monitoring in Bulk on Resource Groups.

- Two types of Resource Groups:

.. table:: Resource Groups

  +-----------------------------------------------------------------+---------------------------------------------------------------+
  | AWS Systems Manager Resource Groups                             | Classic Resource Groups                                       |
  +=================================================================+===============================================================+
  | Has Public API                                                  | No Public API                                                 |
  +-----------------------------------------------------------------+---------------------------------------------------------------+
  | Regional; all resources in a group that you create with AWS     | Cross-regional.                                               |
  | Resource Groups are located in the same region.                 |                                                               |
  +-----------------------------------------------------------------+---------------------------------------------------------------+
  | Permissions are per AWS account                                 | Permissions are per user                                      |
  +-----------------------------------------------------------------+---------------------------------------------------------------+
  | To create a group, you must choose resource types that have at  | To create a group, you must choose a tag key from a drop-down |
  | least one tag key assigned, and specify at least a tag value.   | list. Specifying resource types is optional.                  |
  +-----------------------------------------------------------------+---------------------------------------------------------------+
  | Used to perform tasks such as Systems Manager Automation on     | Used to get monitoring data about resources, such as          |
  | multiple resources at one time; view insights and monitoring    | CloudWatch alarms.                                            |
  | information about grouped resources.                            |                                                               |
  +-----------------------------------------------------------------+---------------------------------------------------------------+
  | You can create a resource group that contains other resource    | You cannot have resource group that contains other resource   |
  | groups in the same region that were created in the new service. | groups.                                                       |
  +-----------------------------------------------------------------+---------------------------------------------------------------+

- Resource Groups have their own navigation Link at top of AWS Console.

AWS CloudTrail
**************

- AWS CloudTrail is an AWS Service that helps you enable governance, compliance, and operational and risk auditing of your AWS account. Actions taken by a user, role, or an AWS service are recorded as events in CloudTrail. Events include actions taken in the AWS Management Console, AWS Command Line Interface, and AWS SDKs and APIs.
- You can view and search the last :magenta:`90 days` of events recorded by CloudTrail in the CloudTrail console or by using the AWS CLI.
- You can download a CSV or JSON file containing up to the past 90 days of CloudTrail events for your AWS account.
- You can create a Trail to deliver log files to your Amazon S3 bucket. By default, when you create a trail in the console, the trail applies to all regions. The trail logs events from all regions in the AWS partition and delivers the log files to the S3 bucket that you specify.
- Process the CloudTrail event logs from S3 bucket using services such as Kinesis for further analysis.

CloudWatch vs CloudTrail
========================

.. table:: CloudWatch vs CloudTrail

  +-------------------------------------------------------------------+--------------------------------------------------------------------+
  | AWS CloudWatch                                                    | AWS CloudTrail                                                     |
  +===================================================================+====================================================================+
  | Tracks performance of AWS resources by collecting metrics in the  | Tracks user activity on **API** calls made through AWS Management  |
  | form of events or logs.                                           | Console, AWS CLI, AWS SDKs and APIs to AWS resources on your       |
  |                                                                   | account.                                                           |
  +-------------------------------------------------------------------+--------------------------------------------------------------------+
  | Reports metrics from 1-minute periods to 5-minute periods.        | Typically delivers log files within 15 minutes of account activity |
  +-------------------------------------------------------------------+--------------------------------------------------------------------+
  | Helps to gain system-wide visibility into resource utilization,   | Helps to gain visibility into your user and resource activity by   |
  | application performance, and operational health.                  | recording AWS API calls.                                           |
  +-------------------------------------------------------------------+--------------------------------------------------------------------+
  | To be used for monitoring the health of your application built on | To be used for monitoring actions performed on the AWS resources   |
  | AWS resources.                                                    | by users on your AWS account.                                      |
  +-------------------------------------------------------------------+--------------------------------------------------------------------+

S3 server logs troubleshooting
******************************

- Provides access log record with the following information about single access requests:

  - Name of the bucket which was accessed (source bucket)
  - Requester
  - Request time
  - Request action
  - Request status and
  - Error code (if any was issued)

- Dump logs of multiple source buckets in a single target bucket.
- Server access log records are delivered on a "best effort basis", i.e. most log records are delivered within a few hours of the time that they were recorded but timeliness of server logging is not guaranteed.
- Object level logging:

  - Logging happens at the object level.
  - When you enable object-level logging in the source bucket, and when a user or application tries to access an object in this bucket, if a CloudTrail trail is setup for the S3 bucket *Data event*, the logs will be created in the target bucket.
  - Leverages CloudTrail service offerings.
  - CloudTrail helps you to audit the activities on the resources in your account.

- Trail and bucket need to be in the same region.

Automation
##########

CloudFormation
**************

What is CloudFormation?

- Provisions, configures and manages the stack of AWS resources (infrastructure) based on the user-given template.
- Infrastructure-As-A-Code (IAAC) service.

Why to use it?

- Simplifies infrastructure management.
- Provides ability to view the design of the resources before provisioning.
- Quickly replicates the infrastructure.
- Controls and tracks infrastructure changes (versioning).
- Handles the dependency between the resources in the template.

.. figure:: images/cf-pipeline.png
   :alt: CloudFormation Pipeline

   A CloudFormation Pipeline

What is a Stack?

- Collection of AWS resources that are created according to the template.
- CloudFormation manages resources by creating, updating or deleting the stacks.
- Can be simple as one resource to an entire web application.
- Must all be created or deleted successfully for the stack to be created or deleted.
- If a resource cannot be created, CloudFormation rolls the stack back and automatically deletes any resources that were created.
- If a resource cannot be deleted, any remaining resources are retained until the stack can be successfully deleted.

Stack Updates

- Upon update, CloudFormation compares the changes submitted with the current state of the stack and **updates only the changed resources**.
- Anyone with stack update permissions can update all of the resources in the stack.
- The progress of a stack update can be monitored by viewing the stack's events.
- Types of updates:

  - Direct Update:

    - Changes deployed immediately.
    - Used for quick deployments.

  - Via Change Sets:

    - Changes can be previewed before deciding whether to apply the changes or not.
    - Used if ensuring only the intentional changes are deployed is important.

- Updates can be cancelled if the stack is still in UPDATE_IN_PROGRESS state.
- Cancelling a stack rolls it back to the stack configuration that existed prior to initiating the stack update.
- Unintentional updates or deletion of stack can be prevented during a stack update by using a stack policy.

Templates

- Contains all the configuration details of the AWS resources to be provisioned via stack.
- JSON or YAML formatted files.
- Template Designer can be used to visualize how the resources will be created and modified.
- Typical template has following sections (sections in bold are important part of template):

  - Format Version - has version date.
  - Description - describes the template via a simple string.
  - Metadata - contains additional data about the template.
  - **Resources** (Mandatory) - a set of resources to be created.
  - **Parameters** - set of custom values.
  - **Mappings** - contains key-value-pairs where keys map to values.
  - **Conditions** - define circumstances in which the resources will be created.
  - **Transform** - specifies macros to process the template.
  - **Outputs** - are the values that can be returned after the stack creation or imported into other stacks as inputs.

Deletion Policy

- Used to specify whether the resource should be deleted, preserved, or backed-up when the stack is deleted.
- Is specified as an attribute in the template.
- If a resource has no DeletionPolicy attribute, CloudFormation deletes the resource by default.
- Options for DeletionPolicy:

  - Delete
  - Retain
  - Snapshot

Elastic BeanStalk labs
**********************

What is Elastic BeanStalk?

- You can quickly deploy and manage applications in the AWS Cloud without worrying about the infrastructure that runs those applications.
- Upload your application, and Elastic BeansStalk automatically handles the details of capacity provisioning, load balancing, scaling, and application health monitoring.
- Elastic BeanStalk supports applications developed in GO, Java, .NET, Node.JS, PHP, Python, and Ruby, as well as different platform configurations for each language on familiar servers such as Apache, Nginx, Passenger, and IIS.
- You can define configuration for infrastructure and software stack to be used for a given environment.
- You can also perform most deployment tasks, such as changing the size of your fleet of Amazon EC2 instances or monitoring your application, directly from the Elastic BeanStalk web interface.

.. figure:: images/beanstalk-pipeline.png
   :alt: BeanStalk pipeline

   BeanStalk Pipeline

Lambda
******

What is AWS Lambda?

- A compute service that lets you run code without provisioning or managing servers. Sometimes also called serverless computing.
- Executes your code only when needed and scales automatically, from a few requests per day to thousands per second.
- Runs your code on a high-availability compute infrastructure and performs all of the administration of the compute resources, including server and operating system maintenance, capacity provisioning and automatic scaling, code monitoring and logging.
- You can write code in following languages:

  - Java,
  - Python,
  - C#,
  - GO, and
  - Node.JS

.. figure:: images/lambda-flow.png
   :alt: Lambda Pipeline Flow

   Lambda Pipeline Flow

- Limitations:

  - Maximum Memory: 3 GB.
  - Maximum execution time: 900 seconds or 15 minutes.

- Lambda can be run in your own VPC, so make sure you have enough capacity for Lambda scaling.

  - .. math::

       \text{capacity &= \text{Projected peak concurrent executions}\cdot\left(\frac{\text{Memory in GB}}{3\text{GB}}\right)\\
       \text{Example:  capacity} &= 1000\cdot\frac{1}{3}\\
                                 &\approx 333\text{ IP Addresses for ENI}

.. figure:: images/lambda-scenarios.png
   :alt: Lambda Scenarios

   Possible Lambda Scenarios

.. code-block:: JSON
   :linenos:
   :caption: IAM Role policy to grant access on S3 source for Lambda

   {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "ListSourceAndDestinationBuckets",
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket",
                "s3:ListBucketVersions"
            ],
            "Resource": [
                "arn:aws:s3:::whiz-demo-lambda-source-bucket",
                "arn:aws:s3:::whiz-demo-lambda-destination-bucket"
            ]
        },
        {
            "Sid": "SourceBucketGetObjectAccess",
            "Effect": "Allow",
            "Action": [
                "s3:GetObject",
                "s3:GetObjectVersion"
            ],
            "Resource": "arn:aws:s3:::whiz-demo-lambda-source-bucket/*"
        },
        {
            "Sid": "DestinationBucketPutObjectAccess",
            "Effect": "Allow",
            "Action": [
                "s3:PutObject"
            ],
            "Resource": "arn:aws:s3:::whiz-demo-lambda-destination-bucket/*"
        },
		 {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "*"
        }
    ]
   }

.. code-block:: Python
   :linenos:
   :caption: Example Python Code for Lambda

   import json
   import boto3
   import os
   from urllib.parse import unquote_plus

   def lambda_handler(event, context):
     # TODO implement

     s3 = boto3.client('s3')

     for record in event['Records']:
        srcKey = unquote_plus(record['s3']['object']['key'])
        srcBucket = record['s3']['bucket']['name']
        print(record)
        print(srcKey)
        destBucket = os.environ['DESTINATION_BUCKET']
        print(destBucket)
        if(srcKey.endswith('/') and record['s3']['object']['size'] == 0):
            print('Object is not a file. Skipping execution...')
        else:
            try:
                print("Starting to copy...")
                copySource = {'Bucket': srcBucket, 'Key':srcKey}

                response = s3.copy_object(CopySource=copySource, Bucket=destBucket, Key=srcKey)

                print("Object {} successfully copied to destination bucket {}".format(srcKey, destBucket))
            except Exception as e:
               print("ERROR: ",e)

OpsWorks
********

What is OpsWorks?

- AWS OpsWorks is a configuration management service that helps you configure and operate applications in a cloud enterprise by using Puppet or Chef.
- Help development and operations teams manage applications and infrastructure.
- AWS OpsWorks for Puppet Enterprise:

  - Lets you create AWS-managed Puppet master servers.
  - A Puppet master server manages nodes in your infrastructure, stores facts about those nodes and serves as a central repository for your Puppet modules.
  - Lets you use Puppet to automate how nodes are configured, deployed and managed, whether they are Amazon EC2 instances or on-premises devices.

- AWS OpsWorks for Chef Automate:

  - Lets you create AWS-managed Chef servers that include Chef Automate premium features, and use the Chef DK and other Chef tooling to manage them.
  - Chef Automate is an included server software package that provides automated workflow for continuous deployment and compliance checks.
  - AWS OpsWorks for Chef Automate installs and manages both the Chef server and Chef Automate by using a single Amazon Elastic Compute Cloud instance.

- AWS OpsWorks Stacks

  - Provides a simple and flexible way to create and manage stacks and lets you deploy and monitor applications in your stacks.
  - You can create stacks that help you manage cloud resources in specialized groups called layers.
  - Monitors instance health, and provisions new instances for you, when necessary, by using Auto Healing and Auto Scaling.

Application Integration
#######################

SQS, SNS and SWF
****************

- SQS (Simple Queue Service)

  - Hosted queue that lets you integrate and decouple distributed software systems and components.
  - Producer component sends message to the queue and consumer component reads/polls message from the queue.
  - You can add message attributes to send structured metadata.
  - Consumer components can poll messages.

    - Short polling

      .. warning:: Problems with short polling:

         #. Queries only subset of SQS Servers.
         #. Need to make lot of ReceiveMessage requests to get all the available messages in the queue.
         #. Might get false empty response.

    - Long polling

      .. note:: Benefits with long polling:

         #. Queries all the SQS servers.
         #. Reduces pricing as it reduces number of Receive Message requests to be made to SQS.
         #. Eliminates false empty responses.

  - You can set visibility timeout, a period of time during which Amazon SQS prevents other consumers from receiving and processing the message (Min: 0 sec, Max: 12 hours, Default: 30 secs).
  - You can use Dead-letter queues, which other queues (source queues) can target for messages that can't be processed (consumed) successfully.
  - Two types of queues:

    - Standard Queue
    - FIFO Queue

  .. figure:: images/sqs.png
     :alt: SQS queues

     SQS Queue Flow

- SNS (Simple Notification Service)

  - Coordinates and manages the delivery or sending of messages to subscribing endpoints or clients.
  - Publishers (producers) communicate asynchronously with subscribers by producing and sending a message to a topic, which is a logical access point and communication channel.
  - Subscribers (consumers) consume or receive the message or notification over one of the supported protocols when they are subscribed to the topic.

    .. figure:: images/sns.png
       :alt: SNS Queue Flow

       SNS Queue Flow

  - Following are SNS subscriber endpoints:

    - HTTP
    - HTTPS
    - eMail
    - eMail - JSON
    - SMS
    - SQS
    - Lambda
    - Applications

  - SNS Scenarios:

    :Fanout: The "fanout" scenario is when an Amazon SNS message is sent to a topic and then replicated and pushed to multiple Amazon SQS queues, HTTP endpoints, or email addresses.

      .. figure:: images/sns-fanout.png
         :alt: SNS Fanout Scenario

         SNS Fanout Scenario

    :Application and System Alerts: Application and system alerts are notifications, triggered by predefined thresholds, sent to specified users by SMS and/or email.

    :Push eMail and Text Messaging: Push email and text messaging are two ways to transmit messages to individuals or groups via email and/or SMS.

    :Mobile Push Notifications: Mobile push notifications enable you to send messages directly to mobile apps.

- SWF (Simple Workflow Service)

  SWF makes it easy to build applications that coordinate work across distributed components.

  :Task: A task represents a logical unit of work that is performed by a component of your application.

  :Worker: Workers are used to perform tasks. These workers can run either on Amazon EC2, or on your own on-premises. Amazon SWF stores tasks and assigns them to workers when they are ready, tracks their progress, and maintains their state, including details on their completion.

  :Actor:

    - **Starter** - any application that can initiate workflow executions.
    - **Deciders** - an implementation of a workflow's coordination logic.
    - **Activity Workers** - a process or thread that performs the activity tasks that are part of your workflow. The activity task represents one of the tasks that you identified in your application.

Summary:

- SQS decouples distributed software systems and components.
- SQS stores messages redundantly on SQS servers.
- To receive SQS messages, Long polling is preferred to short polling.
- Types of queues,

  - Standard Queue
  - FIFO Queue

- SNS is a publisher/subscriber (Pub/Sub) service.
- SNS publisher will push the message to a topic.
- SNS subscriber will subscribe to a topic and will receive message when pushed by publisher.
- SNS scenarios are: Fanout, Application and System Alerts, Push eMail and Text Messaging, and Mobile Push Notifications.
- SWF makes it easy to build applications that coordinate work across distributed components.
- SWF Components are: Tasks, Workers, and Actors (Starters, Deciders, and Activity Workers).
